#!/bin/zsh
for filename in ./*.in; do
    echo "$filename"
    cat "$filename" | (time ../bin/Release/DS_Honors_Opdracht.exe |> "$(basename "$filename" .in).uit") &> "$(basename "$filename" .in).time"
done