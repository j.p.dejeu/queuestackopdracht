﻿using System;

namespace DS_Honors_Opdracht
{
    public class OwnStack<T>
    {
        private int _count;
        private T[] _arr;

        public OwnStack()
        {
            _arr = new T[1];
        }

        public void Push(T item)
        {
            if (_count >= _arr.Length / 2)
            {
                var temp = new T[_arr.Length * 2];
                for (int i = 0; i < _arr.Length; i++)
                {
                    temp[i] = _arr[i];
                }

                _arr = temp;
            }

            _arr[_count++] = item;
        }

        public T Pop()
        {
            var item = _arr[--_count];
            if (_count < _arr.Length / 3)
            {
                var temp = new T[_arr.Length / 2];
                for (var i = 0; i < temp.Length; i++)
                {
                    temp[i] = _arr[i];
                }

                _arr = temp;
            }


            return item;
        }


        public int Count => _count;
    }

    public class OwnQueue<T>
    {
        private OwnStack<T> _inStack = new OwnStack<T>();
        private OwnStack<T> _outStack = new OwnStack<T>();
        private int _count;

        public void Enqueue(T item)
        {
            _inStack.Push(item);
            _count++;
        }

        public T Dequeue()
        {
            if (_outStack.Count == 0)
            {
                while (_inStack.Count != 0)
                {
                    _outStack.Push(_inStack.Pop());
                }
            }

            _count--;
            return _outStack.Pop();
        }

        public int Count => _count;
    }

    internal class Program
    {
        public static void Main()
        {
            var s = new OwnStack<string>();
            var q = new OwnQueue<string>();

            string read;
            while ((read = Console.ReadLine()) != null)
            {
                // Console.WriteLine(string.Join(" ", s.ToArray()));
                // Console.WriteLine(string.Join(".", q.ToArray()));
                var st = read.Split(' ');
                var begin = st[0];
                if (begin == "Aankomst")
                {
                    q.Enqueue(st[1]);
                }
                else if (begin.Equals("Wassenklaar"))
                {
                    s.Push(q.Dequeue());
                }
                else if (begin.Equals("Pakrequest"))
                {
                    Console.WriteLine(s.Pop());
                }
                else if (begin.Equals("Koffie"))
                {
                    Console.WriteLine(q.Count);
                }
            }

            while (q.Count != 0)
            {
                s.Push(q.Dequeue());
            }

            while (s.Count != 0)
            {
                Console.WriteLine(s.Pop());
            }
        }
    }
}