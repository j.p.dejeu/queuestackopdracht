# QueueStack
> Een honours opdracht door Julius de Jeu

## De relevante bestanden
* `latex/QueueStack.tex` - de source voor de PDF instructie
* `tests/`
    * `*.in` - de input die DomJudge aan de programma's geeft
    * `*.uit` - de output die DomJudge verwacht
    * `*.time` - de tijd die het duurde om het programma uit te voeren. Het gaat hierbij om op de 1e regel de eerste `0.00s`, dit is de tijd die het duurde om het uit te voeren met mijn implementatie.
* `Program.cs` - Mijn implementatie die gemiddeld O(1) stack en queue heeft.
* `domjudge/runscript.sh` - Het aangepaste runscript voor DomJudge wat ook kijkt of er nergens `Collections.Generic` gebruikt wordt, daar staat namelijk de "normale" queue en stack implementatie in.